// Fill out your copyright notice in the Description page of Project Settings.BlueprintSpawnableComponent

#pragma once

#include "CoreMinimal.h"
#include "TankAimingComponent.generated.h"

// Enum for aiming state
UENUM()
enum class EFiringState : uint8
{
	Reloading,
	Aiming,
	Locked,
	Winchester
};

 // Forward Declaration
class UTankBarrel;
class UTankTurret;
class AProjectile;

/**
 * Governs the Tanks turret rotation and barrel rotation towards the point of aim.
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialize(UTankBarrel* TankBarrelToSet, UTankTurret* TankTurretToSet);

public:	

	virtual void BeginPlay() override;

	void AimAt(FVector HitLocation);
	void MoveBarrelTowards(FVector AimDirection);
	void MoveTurretTowards(FVector AimDirection);
	
	UFUNCTION(BlueprintCallable, Category = "TankControl")
	void Fire();

	EFiringState GetFiringState() const;
	int32 GetAmmoCount() const;

protected:

	UPROPERTY(BlueprintReadOnly, Category = "State")
	EFiringState FiringState = EFiringState::Aiming;

	UPROPERTY(BlueprintReadOnly, Category = "State")
	int32 AmmoCounter = 3;

private:

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	bool IsBarrelMoving();

	FVector AimDirection;

	// Sets default values for this component's properties
	UTankAimingComponent();

	UTankBarrel* Barrel = nullptr;
	
	UTankTurret* Turret = nullptr;


	//Firing

	UPROPERTY(EditAnywhere, Category = "Weapons")
	TSubclassOf<AProjectile> ProjectileBlueprint;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float ReloadTimeInSeconds = 3.f;

	double LastFireTime = 0;
};
