// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Tank Setup")
		void SetBarrelCollision();

	// -1 is max downward speed, and +1 is max upward speed
	void Elevate(float RelativeSpeed);

private:

	UPROPERTY(EditDefaultsOnly, Category = "Properties")
		float MaxDegreesPerSecond = 5.f; // sensible default

	UPROPERTY(EditDefaultsOnly, Category = "Properties")
		float MaxElevation = 25.f;

	UPROPERTY(EditDefaultsOnly, Category = "Properties")
		float MinElevation = -1.f;
	
};
