// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTurret.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Tank Setup")
		void SetTurretCollision();

	UPROPERTY(EditDefaultsOnly, Category = "Properties")
		float MuzzleVelocity = 77300.f;

	float GetMuzzleVelocity();

	// -1 is max downward speed, and +1 is max upward speed
	void Rotate(float RelativeSpeed);
	
private:

	UPROPERTY(EditDefaultsOnly, category = "Properties")
		float MaxRotateSpeed = 10.f;
};
