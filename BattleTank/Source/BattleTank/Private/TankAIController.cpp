// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "TankAimingComponent.h"
#include "Engine/World.h"

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto ControlledTank = GetPawn();
	auto ControlledAimingController = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	auto PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();

	if (!ensureAlways(PlayerTank)) { return; }
	
	// Move towards the player
	MoveToActor(PlayerTank, AcceptanceRadius);

	//Aim towards player
	ControlledAimingController->AimAt(PlayerTank->GetActorLocation());

	// Fire if ready
	if (ControlledAimingController->GetFiringState() == EFiringState::Locked)
	{
		ControlledAimingController->Fire(); //TODO Fix firing
	}
	

	
}
