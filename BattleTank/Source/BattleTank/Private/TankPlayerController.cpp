// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "Engine/World.h"
#include "Tank.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	PrimaryActorTick.bCanEverTick = true;

	auto ControlledTank = GetControlledTank();

	AimingComponent = GetControlledTank()->FindComponentByClass<UTankAimingComponent>();
	if (!ensureAlways(AimingComponent)) { return; }

	FoundAimingComponent(AimingComponent);

}

void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();

}

ATank* ATankPlayerController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}

void ATankPlayerController::AimTowardsCrosshair()
{
	if (!ensureAlways(AimingComponent)) { return; }
	if (!ensureAlways(GetPawn())) { return; }

	FVector HitLocation; // Out parameter
	if (GetSightRayHitLocation(OUT HitLocation))
	{
		AimingComponent->AimAt(HitLocation);
	}
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& OUTHitLocation) const
{

	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(
		OUT ViewportSizeX,
		OUT ViewportSizeY
	);

	auto ScreenLocation = FVector2D(ViewportSizeX * CrossHairXLocation, ViewportSizeY * CrossHairYLocation);
	//UE_LOG(LogTemp, Warning, TEXT("ScreenLocation: %s"), *ScreenLocation.ToString());

	FVector LookDirection;
	FVector LookLocation;
	FVector HitLocation;

	if (GetLookDirection(ScreenLocation, OUT LookDirection)) {
		//UE_LOG(LogTemp, Warning, TEXT("LookDirection: %s"), *LookDirection.ToString());
		if (GetLookVectorHitLocation(
			LookDirection,
			OUT HitLocation
		)) {
			OUTHitLocation = HitLocation;
			return true;
			//UE_LOG(LogTemp, Warning, TEXT("HitLocation: %s"), *HitLocation.ToString());
		}
	}

	return false;
}

bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& OUTLookDirection) const
{
	FVector WorldLocation;

	DeprojectScreenPositionToWorld(
		ScreenLocation.X,
		ScreenLocation.Y,
		OUT WorldLocation,
		OUT OUTLookDirection
	);
	return true;
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector LookDirection, FVector& OUTHitLocation) const
{
	FHitResult HitResult;
	auto StartLocation = PlayerCameraManager->GetCameraLocation();
	auto LineTraceEnd = StartLocation + (LookDirection * SightRange);
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());
	if (TraceSight) {
		const FName TraceTag("MyTraceTag");
		GetWorld()->DebugDrawTraceTag = TraceTag;
		TraceParameters.TraceTag = TraceTag;
	}
	
	GetWorld()->LineTraceSingleByChannel(
		OUT HitResult,
		StartLocation,
		LineTraceEnd,
		ECollisionChannel::ECC_Visibility,
		TraceParameters
	);
	if (HitResult.GetActor() != nullptr) 
	{
		OUTHitLocation = HitResult.Location;
		return true;
	}
	OUTHitLocation = FVector(0);
	return true;
}
