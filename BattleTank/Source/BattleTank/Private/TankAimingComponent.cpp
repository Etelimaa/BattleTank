// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"


void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	if ((FPlatformTime::Seconds() - LastFireTime) < ReloadTimeInSeconds && FiringState != EFiringState::Winchester) 
	{
		FiringState = EFiringState::Reloading;
	}
	else if (IsBarrelMoving() && FiringState != EFiringState::Winchester)
	{
		FiringState = EFiringState::Aiming;
	}
	else if(FiringState != EFiringState::Winchester)
	{
		FiringState = EFiringState::Locked;
	}
}

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UTankAimingComponent::Initialize(UTankBarrel * TankBarrelToSet, UTankTurret * TankTurretToSet)
{
	Barrel = TankBarrelToSet;
	Turret = TankTurretToSet;
}

void UTankAimingComponent::BeginPlay()
{
	//Initial reload
	LastFireTime = FPlatformTime::Seconds();
}

void UTankAimingComponent::AimAt(FVector HitLocation)
{
	if (!ensureAlways(Barrel && Turret)) {	return;	}

	FVector ProjectileLaunchVector(0);
	FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));
	float Radius = 7.74f / 2;
	float LaunchSpeed = Turret->GetMuzzleVelocity();
	FCollisionResponseParams ResponseParams = ECollisionResponse::ECR_MAX;
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(GetOwner());
	bool bGotProjectileLaunchVector = UGameplayStatics::SuggestProjectileVelocity(
		this,
		OUT ProjectileLaunchVector,
		StartLocation,
		HitLocation,
		LaunchSpeed,
		false,
		0.f,
		0.f,
		ESuggestProjVelocityTraceOption::DoNotTrace,		//ESuggestProjVelocityTraceOption::TraceFullPath, //TODO Remove trace
		ResponseParams,
		IgnoredActors,
		false
	);
	if(bGotProjectileLaunchVector)
	{
		AimDirection = ProjectileLaunchVector.GetSafeNormal();
		MoveBarrelTowards(AimDirection);
		MoveTurretTowards(AimDirection);
	}
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensureAlways(Barrel)) { return false; }

	auto BarrelForward = Barrel->GetForwardVector();
	return  !BarrelForward.Equals(AimDirection, 0.01);
}

void UTankAimingComponent::MoveBarrelTowards(FVector AimDirection)
{
	if (!ensureAlways(Barrel && Turret)) { return; }
	//Work out difference between current barrel rotation, and AimDirection
	FRotator BarrelRotator = Barrel->GetForwardVector().Rotation();
	FRotator AimAsRotator = AimDirection.Rotation();
	FRotator DeltaRotator = AimAsRotator - BarrelRotator;

	Barrel->Elevate(DeltaRotator.Pitch);
}

void UTankAimingComponent::MoveTurretTowards(FVector AimDirection)
{
	if (!ensureAlways(Turret)) { return; }
	FRotator TurretRotator = Turret->GetForwardVector().Rotation();
	FRotator AimAsRotator = AimDirection.Rotation();
	FRotator DeltaRotator = AimAsRotator - TurretRotator;
	//Always yaw the shortest way
	if (DeltaRotator.Yaw > 180) 
	{ 
		DeltaRotator *= -1; 	
	}

	Turret->Rotate(DeltaRotator.Yaw);
}

void UTankAimingComponent::Fire()
{
	if(FiringState != EFiringState::Reloading || FiringState != EFiringState::Winchester)
	{
		if (!ensureAlways(Barrel && ProjectileBlueprint)) { return; }
		//auto SocketLocation = TankBarrel->GetSocketLocation(FName("Projectile"));
		//auto SocketRotation = TankBarrel->GetSocketRotation(FName("Projectile"));

		//Spawn a projectile at the socket location
		// Spawn a projectile at the socket location of the barrel
			auto SocketLocation = Barrel->GetSocketLocation(FName("Projectile"));
			auto SocketRotation = Barrel->GetSocketRotation(FName("Projectile"));

		//GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint, SocketLocation, SocketRotation);


			auto Projectile = GetWorld()->SpawnActor<AProjectile>(
				ProjectileBlueprint,
				SocketLocation,
				SocketRotation
			);

			Projectile->LaunchProjectile(Turret->GetMuzzleVelocity());
			LastFireTime = FPlatformTime::Seconds();
			AmmoCounter--;
			if (AmmoCounter == 0) {
				FiringState = EFiringState::Winchester;
			}

	}
}

EFiringState UTankAimingComponent::GetFiringState() const
{
	return FiringState;
}

int32 UTankAimingComponent::GetAmmoCount() const
{
	return AmmoCounter;
}

